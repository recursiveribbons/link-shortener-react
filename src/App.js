import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import API from './API.js';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="w3-container w3-teal">
          <img src={logo} className="App-logo" alt="logo" />
          <h1>Link Shortener</h1>
        </header>
          <LinkTable />
      </div>
    );
  }
}

class LinkTable extends Component {
    constructor() {
        super();
        this.state = {links:[{linkId: '', linkURL: ''}], apiLinks:[{linkId: '', linkURL: ''}]};
    }

    componentDidMount() {
        API.getLinks()
            .then(result=>result.json())
            .then(items=>this.setState({links:items, apiLinks:items}))
            .catch(result => {
                console.log(result);
                alert("Cannot connect to server");
            });
    }

    handleChange(event, linkId) {
        let newURL = event.target.value;
        let links = this.state.links.slice();
        let i = links.findIndex(x => x.linkId === linkId);
        let link = {linkId: linkId, linkURL: newURL};
        API.editLink(link).then(result => {
            if(result.ok) {
                links[i].linkURL = newURL;
                this.setState({links:links, apiLinks: links});
            } else {
                console.log(result);
                alert("API call failed");
            }
        }).catch(() => alert("API call failed seriously"));
    }

    handleDelete(linkId) {
        let links = this.state.links.slice();
        let i = links.findIndex(x => x.linkId === linkId);
        API.deleteLink(linkId)
            .then(result => {
                if(result.ok) {
                    links.splice(i, 1);
                    this.setState({links:links, apiLinks: links});
                } else {
                    console.log(result);
                    alert("API call failed");
                }
            })
            .catch(() => alert("API call failed seriously"));
    }

    handleAdd(link) {
        let links = this.state.links.slice();
        API.addLink(link)
            .then(result => {
                if(result.ok) {
                    links.push(link);
                    this.setState({links:links, apiLinks: links});
                } else {
                    console.log(result);
                    alert("API call failed");
                }
            })
            .catch(() => alert("API call failed seriously"));
    }

    render() {
        return (
            <table className="w3-table w3-bordered">
                <thead>
                <tr>
                    <th>Link ID</th>
                    <th>Link URL</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                {this.state.links.map((link) =>
                    <LinkRow key={link.linkId} value={link} onChange={(event) => this.handleChange(event, link.linkId) } onClick={() => this.handleDelete(link.linkId)} />
                )}
                </tbody>
                <tfoot>
                <NewLink onAdd={link => this.handleAdd(link)} />
                </tfoot>
            </table>
        );
    }
}

function LinkRow(props) {
    return (
        <tr>
            <td>{props.value.linkId}</td>
            <td><input type='text' value={props.value.linkURL} onChange={props.onChange} /></td>
            <td><button className='w3-button w3-circle w3-black' onClick={props.onClick}>×</button></td>
        </tr>
    );
}

class NewLink extends Component {
    constructor() {
        super();
        this.state = {link: {linkId: "", linkURL: ""}};
    }

    handleChange(event, type) {
        let oldLink = this.state.link;
        let value = event.target.value;
        if(type === 'linkId') {
            this.setState({link: {linkId: value, linkURL: oldLink.linkURL}});
        } else if(type === 'linkURL') {
            this.setState({link: {linkId: oldLink.linkId, linkURL: value}});
        }
    }

    handleAdd() {
        this.props.onAdd(this.state.link)
        this.setState({link: {linkId: "", linkURL: ""}});
    }

    render() {
        return (
            <tr>
                <td><input type='text' name='linkId' value={this.state.link.linkId} onChange={(event) => this.handleChange(event, "linkId")} /></td>
                <td><input type='text' name='linkURL' value={this.state.link.linkURL} onChange={(event) => this.handleChange(event, "linkURL")} /></td>
                <td><button className='w3-button w3-circle w3-teal' onClick={() => this.handleAdd()}>+</button></td>
            </tr>
        );
    }
}

export default App;
