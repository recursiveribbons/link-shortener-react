class API {
    static getBaseURL() {
        return 'http://robintest.cf/links/';
    }

    static getLinks() {
        return fetch(this.getBaseURL());
    }

    static addLink(link) {
        let url = this.getBaseURL();
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return fetch (url, {
            headers: headers,
            method: 'POST',
            body: JSON.stringify(link)
        });
    }

    static editLink(link) {
        let url = this.getBaseURL() + link.linkId;
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return fetch (url, {
            headers: headers,
            method: 'PUT',
            body: JSON.stringify(link)
        });
    }

    static deleteLink(linkId) {
        let url = this.getBaseURL() + linkId;
        return fetch (url, {method: 'DELETE'});
    }
}

export default API;