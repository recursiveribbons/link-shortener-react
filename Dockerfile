FROM node:8.11.3-alpine as builder
RUN npm i npm@latest -g
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./ /app/
RUN npm run build

FROM node:8.11.3-alpine
COPY --from=builder /app/build/ /build
COPY /nginx.conf /etc/nginx/conf.d/default.conf
RUN npm install -g serve
CMD ['serve', '-s', 'build']